<?php

/* ----------------------------------------------------------------------------------
*
* Xaseco plugin paying coppers to first three ranked players.
*
* ----------------------------------------------------------------------------------
*
* Author:           ML aka RookieNouse aka nouseforname @ http://www.tm-forum.com
* Home:             http://nouseforname.de
* Date:             12.05.2012
* Version:          1.2.1
* Dependencies:     none
*
* ----------------------------------------------------------------------------------
*
* LICENSE: This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as published
* by the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*
* ----------------------------------------------------------------------------------
*/
 
Aseco::registerEvent('onSync', 'nouseWinningUpToDate');
Aseco::registerEvent('onEndRace', 'nouseWinning');

function nouseWinningUpToDate($aseco) {
    $aseco->plugin_versions[] = array(
        'plugin'  => 'plugin.nouse.winning.php',
        'author'  => 'Nouseforname',
        'version' => '1.2.1'
    );
}

function nouseWinning ($aseco, $race) {
    // Load settings
    $settingsXML = simplexml_load_file('nouse_winning.xml');
    $limit = intval($settingsXML->limit);
    $minservercoppers = intval($settingsXML->minservercoppers);
    $rankPayouts = array(
        intval($settingsXML->firstplace),
        intval($settingsXML->secondplace),
        intval($settingsXML->thirdplace)
    );
    $rocOnly = intval($settingsXML->roconly);
    $ignores = array();
    foreach ($settingsXML->ignoredplayers->ignoredlogin as $login) {
        $ignores[] = $login;
    }
    
/* ================================================================================= */
    if ($aseco->server->getGame() == 'TMF') {
        // check for TMUF server
        if ($aseco->server->rights) {
            $aseco->client->query('GetLadderServerLimits');
            $ladderlimits = $aseco->client->getResponse();
            $ladder = $ladderlimits['LadderServerLimitMax'];
            if ($rocOnly && $ladder != 100000) {
                return;
            }
        } else {
            $message = formatText($aseco->getChatMessage('UNITED_ONLY'), 'server');
            $this->Aseco->client->addCall('ChatSendServerMessage', array($aseco->formatColors($message)));
            return;
        }
    } else {
        $message = $aseco->getChatMessage('FOREVER_ONLY');
        $aseco->client->addCall('ChatSendServerMessage', array($aseco->formatColors($message)));
        return;
    }

    $aseco->client->query('GetServerCoppers');
    $coppers = $aseco->client->getResponse();
    if ($coppers >= $minservercoppers) {
        $ranks = array_filter($race[0], function ($rank) {
            return $rank['BestTime'] > 0 || $rank['Score'] > 0;
        });
        // check if enough players finished track
        if (count($ranks) >= $limit) {
            for ($i = 0; $i <= 2; $i++) {
                if (isset($ranks[$i]) && $rankPayouts[$i]) {
                    $rights = $aseco->server->players->getPlayer($ranks[$i]['Login'])->rights;
                    if ($rights && !in_array($ranks[$i]['Login'], $ignores)) {
                        // Who needs if statements anyway, 0 = 1st, 1 = 2nd, 2 = 3rd
                        $pos = strval($i + 1) . chr(110 + (($i + 1) % 2 * 4) + ($i == 0)) . chr(100 + (($i == 0) * 16));
                        $message = '$ff0> ' . $ranks[$i]['NickName'] . ' $g$z$6c0$s$ifinished at ' . $pos . ' position and won ' . $rankPayouts[$i] . ' coppers !!!';
                        $aseco->client->addCall('ChatSendServerMessage', array($aseco->formatColors($message)));
                        $message = 'You finished at ' . $pos . ' position and won ' . $rankPayouts[$i] . ' coppers!';
                        $aseco->client->addCall('Pay', array($ranks[$i]['Login'], (int)$rankPayouts[$i], $aseco->formatColors($message)));
                    }
                }
            }
        } else {
            $message = '$ff0> $g$z$6c0$s$i Not enough players finished track, Winning payment disabled!!!';
            $aseco->client->addCall('ChatSendServerMessage', array($aseco->formatColors($message)));
        }
    } else {
        $message = '$ff0> $g$z$6c0$s$iServer coppers too low, winning payment disabled. Please donate some.';
        $aseco->client->addCall('ChatSendServerMessage', array($aseco->formatColors($message)));
    }
}
